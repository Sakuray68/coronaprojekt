#' Einzelne Regionen aus den Rohdaten auswählen
#'
#' Diese Funktion erstellt einen Tibble mit allen Regionen, die man ausgewählt
#' hat. Um Regionen auszuwählen, muss man sie in einem einzigen String angeben,
#' der mit ", " getrennt ist (Bitte auch auf das Leerzeichen achten).
#'
#' Werden einzelne Landkreise oder Bundesländer angegeben, so erhält man auch
#' nur diese genannten Regionen zurück.
#' Werden Landkreise und Bundesländer vermischt, so erhält man ein Tibble mit
#' allen ausgewälten Landkreisen und allen übrigen Landkreisen in den genannten
#' Bundesländern.
#'
#' Um neben den Regionen und den Fallzahlen noch weitere Spalten aus der
#' Orginaltabelle zu behalten, muss man diese nur als weitere Argumente angeben.
#' So kann man auch noch "Landkreis" als weiteres Argument angeben, um die
#' Bundesländer auch nach Landkreisen aufzulösen.
#'
#' __TIPP__: Möchte man alle Landkreise aus einzelnen Bundesländern haben, ist der
#' Zusatzparameter "Landkreis" schneller als die Angabe aller Landkreise einzeln.
#' Selbiges gilt für die Auswahl aller Bundesländer oder gar aller Landkreise
#' aus ganz Deutschland (Region = "gesamt").
#'
#' @param Region String mit den gewünschten Regionen, getrennt durch ", ". Default = "gesamt".
#' @param ... Weitere Spalten aus der Rohdatentabelle, die übernommen werden sollen.
#'
#' @return Einen Tibble mit den angegebenen Spalten und den ausgewälten Regionen.
#'
#' @examples
#' cov_convert(Region="gesamt")
#' cov_convert(Region="Berlin, Brandenburg", Altersgruppe, Geschlecht)
#' cov_convert(Region="Bayern, Baden-Württemberg", Landkreis, Geschlecht)
#' cov_convert(Region="SK Heidelberg, SK Mannheim, SK Karlsruhe, SK Stuttgart")
#'
#' @export
cov_convert <- function(Region="gesamt", ...){

  # tibble r vorbereiten um die Daten entgegen nehmen zu können
  r <- filter(tbl_raw_data, Bundesland == " ")

  kreis <- F
  land <- F

  # Filtern der Region (default = gesamt Deutschland)
  if (Region == "gesamt"){
    r <- tbl_raw_data
  } else if (str_detect(Region, ",")) {
    Region <- str_split_fixed(Region, ", ",n=Inf)
    for (reg in Region) {
      temp <- filter(tbl_raw_data, Landkreis == reg )
      if (dim(temp)[1]>0) {
        kreis <- TRUE
        r <- add_row(r,temp)
      }
      temp <- filter(tbl_raw_data, Bundesland == reg)
      if (dim(temp)[1]>0) {
        land <- TRUE
        r <- add_row(r,temp)
      }
    }
    if (!kreis && !land) {
      warning("Keine der gewünschten Regionen konnte gefunden werden.\nTabelle bleibt leer")
    }
  } else {
    temp <- filter(tbl_raw_data, Bundesland == Region)
    if (dim(temp)[1]>0) {
      land <- TRUE
      r <- add_row(r,temp)
    }
    temp <- filter(tbl_raw_data, Landkreis == Region)
    if (dim(temp)[1]>0) {
      kreis <- TRUE
      r <- add_row(r,temp)
    }
    if (!kreis && !land) {
      warning("Gewünschte Region konnte nicht gefunden werden.\nTabelle bleibt leer")
    }
  }

  # Gruppen erstellen
  gruppen <- enquos(...)
  if (land&&kreis) {
    r <- group_by(r, Meldedatum, Landkreis, Bundesland, !!!gruppen)
  } else if (land) {
    r <- group_by(r, Meldedatum, Bundesland, !!!gruppen)
  } else if (kreis) {
    r <- group_by(r, Meldedatum, Landkreis, !!!gruppen)
  } else {
    r <- group_by(r, Meldedatum, !!!gruppen)
  }

  # Fälle aufsummieren
  r %>%
    unique() %>%
    summarise(
      AnzahlFall = sum(AnzahlFall),
      AnzahlTodesfall = sum(AnzahlTodesfall),
      AnzahlGenesen = sum(AnzahlGenesen)
    ) %>%
    unique() %>%
    mutate(Meldedatum=as.Date(Meldedatum))
}

#' Berechnet den gleitenden Mittelwert über \code{timeframe} - Tage
#'
#' Diese Funktion berechnet den gleitenden Mittelwert für die Regionen über
#' einen Zeitraum von \code{timeframe} Tagen. Weitere Spalten, wie Geschlecht
#' oder Altersgruppen werden hierbei ignoriert und zusammengefügt
#'
#' @param tble Tibble mit den Regionen, über die der Mittelwert berechnet werden soll
#' @param Level Administratives Level, das die Regionen darstellt ("Landkreis", "Bundesland", oder "gesamt")
#' @param timeframe Anzahl der Tage, über die gemittelt werden soll (Default = 7)
#' @param keep_NA Sollen die Zeilen mit NA-Werten beibehalten werden, die bei der Rechnung entstehen? (Default = FLASE)
#'
#' @return Tibble mit gemittelten Werten. Spalten: Meldedatum, (Landkreis/Bundesland), Fallzahlen, Todeszahlen, Genesenenzahlen
#'
#' @seealso \code{cov_MittelwertGeschlecht()}
#' @seealso \code{cov_MittelwertAltersgruppe()}
#'
#' @example
#' tabelle <- cov_convert(Region = "Bayern", Landkreis)
#' cov_MittelwertRegion(tabelle)
#'
#' @export
cov_MittelwertRegion <- function(tble, Level = "Landkreis", timeframe = 7, keep_NA = FALSE) {
  #Eingabevalidierung
  stopifnot("Level muss entweder 'Landkreis', 'Bundesland', oder 'gesamt' sein" = ((Level=="Landkreis")|(Level=="Bundesland"))|(Level=="gesamt"))
  stopifnot("timeframe muss ein positives Integer sein" = ((class(timeframe) == "numeric") && (timeframe>0)) && (timeframe %% 1 == 0))
  stopifnot("keep_NA muss TRUE oder FALSE sein" = class(keep_NA) == "logical" )
  # Fehlende Tage einfügen
  cat("Fehlende Tage werden aufgefüllt...\n")
  tble %>%
    ungroup() %>%
    select(Meldedatum) %>%
    summarise(
      min=min(Meldedatum),
      max=max(Meldedatum)
    ) -> minmaxDays
  reg <- 0 # finde alle Regionen
  if ((Level=="Landkreis")||(Level=="Bundesland")) {
    tble %>%
      ungroup() %>%
      select(!!Level) %>%
      unique() -> reg
  }
  nrDays <- as.integer(as.Date(minmaxDays[[2]]))-as.integer(as.Date(minmaxDays[[1]]))
  tblDays <- 0 # erstelle Tibble mit allen Tagen pro Region
  if("tbl" %in% class(reg)) {
    tblDays <- tibble(
      Datum = rep(as.Date(minmaxDays[[1]])+ 0:nrDays, times = length(reg[[1]])),
      Region = rep(reg[[1]], each = nrDays+1)
    )
  } else {
    tblDays <- tibble(
      Datum = rep(minmaxDays[[1]]+0:nrDays)
    )
  }
  tble$Meldedatum <- as.Date(tble$Meldedatum)
  # joine beide tabellen, um die fehlenden Tage in der Berechnung nicht aus zu lassen
  if (Level == "Landkreis") {
    tble <- full_join(tble,tblDays, by= ( c("Meldedatum"="Datum", "Landkreis"="Region") ))
  } else if (Level == "Bundesland") {
    tble <- full_join(tble,tblDays, by= ( c("Meldedatum"="Datum", "Bundesland"="Region") ))
  } else {
    tble <- full_join(tble, tblDays, by=(c("Meldedatum"="Datum")))
  }
  tble %>%
    mutate(
      AnzahlFall = replace(AnzahlFall, is.na(AnzahlFall), 0),
      AnzahlTodesfall = replace(AnzahlTodesfall, is.na(AnzahlTodesfall), 0),
      AnzahlGenesen= replace(AnzahlGenesen, is.na(AnzahlGenesen), 0)
    ) -> tble

  # RollAvg berechnen
  cat("Mittelwerte werden berechnet...\n")
  if (Level == "Landkreis") {
    tble %>%
      ungroup() %>%
      group_by(Meldedatum, Landkreis) %>%
      summarise(
        AnzahlFall = sum(AnzahlFall),
        AnzahlTodesfall = sum(AnzahlTodesfall),
        AnzahlGenesen = sum(AnzahlGenesen)
      ) %>%
      ungroup() %>%
      unique() %>%
      arrange(desc(Landkreis)) %>%
      group_by(Landkreis) %>%
      mutate(
        Landkreis=Landkreis,
        Meldedatum = Meldedatum,
        AnzahlFall = AnzahlFall,
        Fallzahlen = rollmean(AnzahlFall, k=timeframe, fill = NA),
        Todeszahlen = rollmean(AnzahlTodesfall, k=timeframe, fill = NA),
        Genesenenzahlen = rollmean(AnzahlGenesen, k=timeframe, fill = NA)
      ) %>%
      select(Meldedatum, Landkreis, Fallzahlen, Todeszahlen, Genesenenzahlen) %>%
      ungroup() -> result
  } else if (Level=="Bundesland"){ # ELSE IF Level = Bundesland
    tble %>%
      ungroup() %>%
      group_by(Meldedatum, Bundesland) %>%
      summarise(
        AnzahlFall = sum(AnzahlFall),
        AnzahlTodesfall = sum(AnzahlTodesfall),
        AnzahlGenesen = sum(AnzahlGenesen)
      ) %>%
      ungroup() %>%
      unique() %>%
      arrange(desc(Bundesland)) %>%
      group_by(Bundesland) %>%
      mutate(
        Bundesland=Bundesland,
        Meldedatum = Meldedatum,
        AnzahlFall = AnzahlFall,
        Fallzahlen = rollmean(AnzahlFall, k=timeframe, fill = NA),
        Todeszahlen = rollmean(AnzahlTodesfall, k=timeframe, fill = NA),
        Genesenenzahlen = rollmean(AnzahlGenesen, k=timeframe, fill = NA)
      ) %>%
      select(Meldedatum, Bundesland, Fallzahlen, Todeszahlen, Genesenenzahlen) %>%
      ungroup() -> result
  } else { # ELSE Level = Bundesebene ("gesamt")
    tble %>%
      ungroup() %>%
      group_by(Meldedatum) %>%
      summarise(
        AnzahlFall = sum(AnzahlFall),
        AnzahlTodesfall = sum(AnzahlTodesfall),
        AnzahlGenesen = sum(AnzahlGenesen)
      ) %>%
      ungroup() %>%
      unique() %>%
      arrange(Meldedatum) %>%
      mutate(
        Meldedatum = Meldedatum,
        AnzahlFall = AnzahlFall,
        Fallzahlen = rollmean(AnzahlFall, k=timeframe, fill = NA),
        Todeszahlen = rollmean(AnzahlTodesfall, k=timeframe, fill = NA),
        Genesenenzahlen = rollmean(AnzahlGenesen, k=timeframe, fill = NA)
      ) %>%
      select(Meldedatum, Fallzahlen, Todeszahlen, Genesenenzahlen) %>%
      ungroup() -> result
  }

  #keep_NA anwenden
  if (keep_NA) {
    return(result)
  } else {
    return(filter(result, !is.na(Fallzahlen)))
  }
}

#' Berechnet den gleitenden Mittelwert über \code{timeframe} - Tage für eine
#' ausgewählte Region
#'
#' Diese Funktion berechnet den gleitenden Mittelwert für die angegebenen Regionen
#' über die einen Zeitraum von \code{timeframe} Tagen. Weitere Spalten, wie
#' Geschlecht oder Altersgruppen werden hierbei ignoriert und zusammengefügt.
#'
#' @param Region String mit den gewünschten Regionen, getrennt durch ", ". Default = "gesamt"
#' @param ... Weitere Spalten aus der Rohdatentabelle, die übernommen werden sollen
#' @param Zeitfenster Anzahl der Tage, über die gemittelt werden soll (Default = 7)
#' @param keep_NA Sollen die Zeilen mit NA-Werten beibehalten werden, die bei der Rechnung entstehen? (Default = FLASE)
#'
#' @return Tibble mit gemittelten Werten. Spalten: Meldedatum, (Landkreis/Bundesland), Fallzahlen, Todeszahlen, Genesenenzahlen
#'
#' @seealso \code{cov_convert()}
#' @seealso \code{cov_MittelwertRegion()}
#' @seealso \code{cov_regionalMittelwertGeschlecht()}
#' @seealso \code{cov_regionalMittelwertAltersgruppe()}
#'
#' @example
#' tabelle <- cov_convert(Region = "Bayern", Landkreis)
#' cov_MittelwertRegion(tabelle)
#'
#' @export
cov_regionalMittelwert <- function(Region = "gesamt", ..., Zeitfenster = 7, keep_NA = FALSE){
  #Eingabevalidierung
  stopifnot("Zeitfenster muss ein positives Integer sein" = ((class(Zeitfenster) == "numeric") && (Zeitfenster>0)) && (Zeitfenster %% 1 == 0))
  stopifnot("keep_NA muss TRUE oder FALSE sein" = class(keep_NA) == "logical" )
  # tibble mit der ausgewählten Region auswählen
  cat("Region aussuchen und gewünschte Filter anwenden...\n")
  suppressWarnings(tble <- ungroup(cov_convert(Region = Region, ...)))

  #Validierung ob Regionen vorhanden sind
  stopifnot("Keine Regionen gefunden" = dim(tble)[1] > 0)

  # Administratives Level bestimmen
  suppressWarnings(
    test <- tble$Landkreis
  )
  if (is.null(test)) {
    suppressWarnings(
      test <- tble$Bundesland
    )
    if (is.null(test)) {
      lev = "gesamt"
    } else { # Bundesland ist nicht NULL
      lev <- "Bundesland"
    }
  } else { # Landkreis ist nicht NULL
    lev <- "Landkreis"
  }
  #Mittelwert berechnen und tibble zurückgeben
  return(
    cov_MittelwertRegion(tble = tble, Level = lev, timeframe = Zeitfenster, keep_NA = keep_NA)
  )
}

#' Berechnet den gleitenden Mittelwert über \code{Zeitfenster} - Tage für die
#' Geschlechter.
#'
#' Diese Funktion berechnet den gleitenden Mittelwert für die Geschlechter
#' über die einen Zeitraum von \code{Zeitfenster} Tagen. Weitere Spalten, wie
#' Region oder Altersgruppen werden hierbei ignoriert und zusammengefügt.
#'
#' @param tble Tibble, über das der Mittelwert berechnet werden soll
#' @param Zeitfenster Anzahl der Tage, über die gemittelt werden soll (Default = 7)
#' @param keep_NA Sollen die Zeilen mit NA-Werten beibehalten werden, die bei der Rechnung entstehen? (Default = FLASE)
#'
#' @return Tibble mit gemittelten Werten. Spalten: Meldedatum, Geschlecht, Fallzahlen, Todeszahlen, Genesenenzahlen
#'
#' @seealso \code{cov_MittelwertRegion()}
#' @seealso \code{cov_MittelwertAltersgruppe()}
#'
#' @example
#' tabelle <- cov_convert(Region = "gesamt", Geschlecht)
#' cov_MittelwertGeschlecht(tabelle)
#'
#' @export
cov_MittelwertGeschlecht <- function(tble, Zeitfenster = 7, keep_NA = FALSE){
  #Eingabevalidierung
  stopifnot("Zeitfenster muss ein positives Integer sein" = ((class(Zeitfenster) == "numeric") && (Zeitfenster>0)) && (Zeitfenster %% 1 == 0))
  stopifnot("keep_NA muss TRUE oder FALSE sein" = class(keep_NA) == "logical" )
  # Fehlende Tage einfügen
  cat("Fehlende Tage werden aufgefüllt...\n")
  tble %>%
    ungroup() %>%
    select(Meldedatum) %>%
    summarise(
      min=min(Meldedatum),
      max=max(Meldedatum)
    ) -> minmaxDays
  tble %>%
    ungroup() %>%
    select(Geschlecht) %>%
    unique() -> reg
  nrDays <- as.integer(as.Date(minmaxDays[[2]]))-as.integer(as.Date(minmaxDays[[1]]))
  tblDays <- tibble(
    Meldedatum = rep(as.Date(minmaxDays[[1]])+0:nrDays, times = length(reg[[1]])),
    Geschlecht = rep(reg[[1]], each = nrDays + 1)
  )
  tble$Meldedatum <- as.Date(tble$Meldedatum)
  # joine beide tabellen, um die fehlenden Tage in der Berechnung nicht aus zu lassen
  tble <- full_join(tble, tblDays)
  tble %>%
    mutate(
      AnzahlFall = replace(AnzahlFall, is.na(AnzahlFall), 0),
      AnzahlTodesfall = replace(AnzahlTodesfall, is.na(AnzahlTodesfall), 0),
      AnzahlGenesen= replace(AnzahlGenesen, is.na(AnzahlGenesen), 0)
    ) -> tble

  cat("Mittelwerte werden berechnet...\n")
  tble %>%
    ungroup() %>%
    group_by(Meldedatum, Geschlecht) %>%
    summarise(
      AnzahlFall = sum(AnzahlFall),
      AnzahlTodesfall = sum(AnzahlTodesfall),
      AnzahlGenesen = sum(AnzahlGenesen)
    ) %>%
    ungroup() %>%
    unique() %>%
    arrange(desc(Geschlecht)) %>%
    group_by(Geschlecht) %>%
    mutate(
      Meldedatum = Meldedatum,
      AnzahlFall = AnzahlFall,
      Fallzahlen = rollmean(AnzahlFall, k=Zeitfenster, fill = NA),
      Todeszahlen = rollmean(AnzahlTodesfall, k=Zeitfenster, fill = NA),
      Genesenenzahlen = rollmean(AnzahlGenesen, k=Zeitfenster, fill = NA)
    ) %>%
    select(Meldedatum, Geschlecht, Fallzahlen, Todeszahlen, Genesenenzahlen) %>%
    ungroup()-> result
  #keep_NA anwenden
  if (keep_NA) {
    return(result)
  } else {
    return(filter(result, !is.na(Fallzahlen)))
  }
}

#' Berechnet den gleitenden Mittelwert über \code{timeframe} - Tage für die
#' Altersgruppen.
#'
#' Diese Funktion berechnet den gleitenden Mittelwert für die Altersgruppen
#' über die einen Zeitraum von \code{timeframe} Tagen. Weitere Spalten, wie
#' Region oder Geschlechter werden hierbei ignoriert und zusammengefügt.
#'
#' @param tble Tibble, über das der Mittelwert berechnet werden soll
#' @param Zeitfenster Anzahl der Tage, über die gemittelt werden soll (Default = 7)
#' @param keep_NA Sollen die Zeilen mit NA-Werten beibehalten werden, die bei der Rechnung entstehen? (Default = FLASE)
#'
#' @return Tibble mit gemittelten Werten. Spalten: Meldedatum, Altersgruppen, Fallzahlen, Todeszahlen, Genesenenzahlen
#'
#' @seealso \code{cov_MittelwertRegion()}
#' @seealso \code{cov_MittelwertGeschlecht()}
#'
#' @example
#' tabelle <- cov_convert(Region = "gesamt", Altersgruppen)
#' cov_MittelwertAltersgruppe(tabelle)
#'
#' @export
cov_MittelwertAltersgruppe <- function(tble, Zeitfenster = 7, keep_NA = FALSE){
  #Eingabevalidierung
  stopifnot("Zeitfenster muss ein positives Integer sein" = ((class(Zeitfenster) == "numeric") && (Zeitfenster>0)) && (Zeitfenster %% 1 == 0))
  stopifnot("keep_NA muss TRUE oder FALSE sein" = class(keep_NA) == "logical" )
  # Fehlende Tage einfügen
  cat("Fehlende Tage werden aufgefüllt...\n")
  tble %>%
    ungroup() %>%
    select(Meldedatum) %>%
    summarise(
      min=min(Meldedatum),
      max=max(Meldedatum)
    ) -> minmaxDays
  tble %>%
    ungroup() %>%
    select(Altersgruppe) %>%
    unique() -> reg
  nrDays <- as.integer(as.Date(minmaxDays[[2]]))-as.integer(as.Date(minmaxDays[[1]]))
  tblDays <- tibble(
    Meldedatum = rep(as.Date(minmaxDays[[1]])+0:nrDays, times = length(reg[[1]])),
    Altersgruppe = rep(reg[[1]], each = nrDays + 1)
  )
  tble$Meldedatum <- as.Date(tble$Meldedatum)
  # joine beide tabellen, um die fehlenden Tage in der Berechnung nicht aus zu lassen
  tble <- full_join(tble, tblDays)
  tble %>%
    mutate(
      AnzahlFall = replace(AnzahlFall, is.na(AnzahlFall), 0),
      AnzahlTodesfall = replace(AnzahlTodesfall, is.na(AnzahlTodesfall), 0),
      AnzahlGenesen= replace(AnzahlGenesen, is.na(AnzahlGenesen), 0)
    ) -> tble

  cat("Mittelwerte werden berechnet...\n")
  tble %>%
    ungroup() %>%
    group_by(Meldedatum, Altersgruppe) %>%
    summarise(
      AnzahlFall = sum(AnzahlFall),
      AnzahlTodesfall = sum(AnzahlTodesfall),
      AnzahlGenesen = sum(AnzahlGenesen)
    ) %>%
    ungroup() %>%
    unique() %>%
    arrange(desc(Altersgruppe)) %>%
    group_by(Altersgruppe) %>%
    mutate(
      Meldedatum = Meldedatum,
      AnzahlFall = AnzahlFall,
      Fallzahlen = rollmean(AnzahlFall, k=Zeitfenster, fill = NA),
      Todeszahlen = rollmean(AnzahlTodesfall, k=Zeitfenster, fill = NA),
      Genesenenzahlen = rollmean(AnzahlGenesen, k=Zeitfenster, fill = NA)
    ) %>%
    select(Meldedatum, Altersgruppe, Fallzahlen, Todeszahlen, Genesenenzahlen) %>%
    ungroup()-> result
  #keep_NA anwenden
  if (keep_NA) {
    return(result)
  } else {
    return(filter(result, !is.na(Fallzahlen)))
  }
}

#' Berechnet den gleitenden Mittelwert über \code{timeframe} - Tage für die
#' Geschlechter der angegebenen Regionen.
#'
#' Diese Funktion berechnet den gleitenden Mittelwert für die Geschlechter
#' über die einen Zeitraum von \code{timeframe} Tagen in den angegebenen Regionen.
#' Weitere Spalten, wie Altersgruppen o.ä. werden hierbei ignoriert
#' und zusammengefügt.
#'
#' @param Region String mit den gewünschten Regionen, getrennt durch ", ". Default = "gesamt"
#' @param ... Weitere Spalten aus der Rohdatentabelle, die übernommen werden sollen
#' @param Zeitfenster Anzahl der Tage, über die gemittelt werden soll (Default = 7)
#' @param keep_NA Sollen die Zeilen mit NA-Werten beibehalten werden, die bei der Rechnung entstehen? (Default = FLASE)
#'
#' @return Tibble mit gemittelten Werten. Spalten: Meldedatum, (Landkreis/Bundesland), Geschlecht, Fallzahlen, Todeszahlen, Genesenenzahlen
#'
#' @seealso \code{cov_convert()}
#' @seealso \code{cov_MittelwertGeschlecht()}
#' @seealso \code{cov_regionalMittelwert()}
#' @seealso \code{cov_regionalMittelwertAltersgruppe()}
#'
#' @example
#' cov_regionalMittelwertGeschlecht(Region = "Rheinland-Pfalz")
#'
#' @export
cov_regionalMittelwertGeschlecht <- function(Region = "gesamt", ..., Zeitfenster = 7, keep_NA = FALSE){
  #Eingabevalidierung
  stopifnot("Zeitfenster muss ein positives Integer sein" = ((class(Zeitfenster) == "numeric") && (Zeitfenster>0)) && (Zeitfenster %% 1 == 0))
  stopifnot("keep_NA muss TRUE oder FALSE sein" = class(keep_NA) == "logical" )
  # tibble mit der ausgewählten Region auswählen
  cat("Region aussuchen und gewünschte Filter anwenden...\n")
  suppressWarnings(tble <- ungroup(cov_convert(Region = Region, ..., Geschlecht)))

  #Validierung ob Regionen vorhanden sind
  stopifnot("Keine Regionen gefunden" = dim(tble)[1] > 0)

  # Administratives Level bestimmen und Regionen rausfiltern
  cat("Regionen auslesen...\n")
  suppressWarnings(
    test <- tble$Landkreis
  )
  if (is.null(test)) {
    suppressWarnings(
      test <- tble$Bundesland
    )
    if (is.null(test)) {
      lev = "gesamt"
    } else { # Bundesland ist nicht NULL
      lev <- "Bundesland"
      regionen <- unique(tble$Bundesland)
    }
  } else { # Landkreis ist nicht NULL
    lev <- "Landkreis"
    regionen <- unique(tble$Landkreis)
  }

  if (lev == "gesamt") {
    #Mittelwert berechnen und tibble zurückgeben
    return(
      cov_MittelwertGeschlecht(tble = tble, Zeitfenster = Zeitfenster, keep_NA = keep_NA)
    )
  } else if (lev == "Landkreis") {
    result <- tibble(
      cov_MittelwertGeschlecht( filter(tble, Landkreis == regionen[1])),
      Landkreis = regionen[1]
    )
    for (i in regionen[-1]) {
      temp <- cov_MittelwertGeschlecht(
        filter(tble, Landkreis == i),
        Zeitfenster = Zeitfenster, keep_NA = keep_NA
        )
      result <- add_row(result,
        tibble(temp,Landkreis = i)
      )
    }
    return(
      select(result, Meldedatum, Landkreis, Geschlecht, Fallzahlen, Todeszahlen, Genesenenzahlen)
    )
  } else { # lev = Bundesland
    result <- tibble(
      cov_MittelwertGeschlecht( filter(tble, Bundesland == regionen[1])),
      Bundesland = regionen[1]
    )
    for (i in regionen[-1]) {
      temp <- cov_MittelwertGeschlecht(
        filter(tble, Bundesland == i),
        Zeitfenster = Zeitfenster, keep_NA = keep_NA
      )
      result <- add_row(result,
                        tibble(temp,Bundesland = i)
      )
    }
    return(
      select(result, Meldedatum, Bundesland, Geschlecht, Fallzahlen, Todeszahlen, Genesenenzahlen)
    )
  }
}

#' Berechnet den gleitenden Mittelwert über \code{timeframe} - Tage für die
#' Altersgruppen der angegebenen Regionen.
#'
#' Diese Funktion berechnet den gleitenden Mittelwert für die Altersgruppen
#' über die einen Zeitraum von \code{timeframe} Tagen in den angegebenen Regionen.
#' Weitere Spalten, wie Geschlecht o.ä. werden hierbei ignoriert
#' und zusammengefügt.
#'
#' @param Region String mit den gewünschten Regionen, getrennt durch ", ". Default = "gesamt"
#' @param ... Weitere Spalten aus der Rohdatentabelle, die übernommen werden sollen
#' @param Zeitfenster Anzahl der Tage, über die gemittelt werden soll (Default = 7)
#' @param keep_NA Sollen die Zeilen mit NA-Werten beibehalten werden, die bei der Rechnung entstehen? (Default = FLASE)
#'
#' @return Tibble mit gemittelten Werten. Spalten: Meldedatum, (Landkreis/Bundesland), Altersgruppe, Fallzahlen, Todeszahlen, Genesenenzahlen
#'
#' @seealso \code{cov_convert()}
#' @seealso \code{cov_MittelwertAltersgruppe()}
#' @seealso \code{cov_regionalMittelwert()}
#' @seealso \code{cov_regionalMittelwertGeschlecht()}
#'
#' @example
#' cov_regionalMittelwertAltersgruppe(Region = "Saarland")
#'
#' @export
cov_regionalMittelwertAltersgruppe <- function(Region = "gesamt", ..., Zeitfenster = 7, keep_NA = FALSE){
  #Eingabevalidierung
  stopifnot("Zeitfenster muss ein positives Integer sein" = ((class(Zeitfenster) == "numeric") && (Zeitfenster>0)) && (Zeitfenster %% 1 == 0))
  stopifnot("keep_NA muss TRUE oder FALSE sein" = class(keep_NA) == "logical" )
  # tibble mit der ausgewählten Region auswählen
  cat("Region aussuchen und gewünschte Filter anwenden...\n")
  suppressWarnings(tble <- ungroup(cov_convert(Region = Region, ..., Altersgruppe)))

  #Validierung ob Regionen vorhanden sind
  stopifnot("Keine Regionen gefunden" = dim(tble)[1] > 0)

  # Administratives Level bestimmen und Regionen rausfiltern
  cat("Regionen auslesen...\n")
  suppressWarnings(
    test <- tble$Landkreis
  )
  if (is.null(test)) {
    suppressWarnings(
      test <- tble$Bundesland
    )
    if (is.null(test)) {
      lev = "gesamt"
    } else { # Bundesland ist nicht NULL
      lev <- "Bundesland"
      regionen <- unique(tble$Bundesland)
    }
  } else { # Landkreis ist nicht NULL
    lev <- "Landkreis"
    regionen <- unique(tble$Landkreis)
  }

  if (lev == "gesamt") {
    #Mittelwert berechnen und tibble zurückgeben
    return(
      cov_MittelwertAltersgruppe(tble = tble, Zeitfenster = Zeitfenster, keep_NA = keep_NA)
    )
  } else if (lev == "Landkreis") {
    result <- tibble(
      cov_MittelwertAltersgruppe( filter(tble, Landkreis == regionen[1])),
      Landkreis = regionen[1]
    )
    for (i in regionen[-1]) {
      temp <- cov_MittelwertAltersgruppe(
        filter(tble, Landkreis == i),
        Zeitfenster = Zeitfenster, keep_NA = keep_NA
      )
      result <- add_row(result,
                        tibble(temp,Landkreis = i)
      )
    }
    return(
      select(result, Meldedatum, Landkreis, Altersgruppe, Fallzahlen, Todeszahlen, Genesenenzahlen)
    )
  } else { # lev = Bundesland
    result <- tibble(
      cov_MittelwertAltersgruppe( filter(tble, Bundesland == regionen[1])),
      Bundesland = regionen[1]
    )
    for (i in regionen[-1]) {
      temp <- cov_MittelwertAltersgruppe(
        filter(tble, Bundesland == i),
        Zeitfenster = Zeitfenster, keep_NA = keep_NA
      )
      result <- add_row(result,
                        tibble(temp,Bundesland = i)
      )
    }
    return(
      select(result, Meldedatum, Bundesland, Altersgruppe, Fallzahlen, Todeszahlen, Genesenenzahlen)
    )
  }
}
