#' Impfquote
#'
#' Anteil der mindestens einmal sowie der vollständig geimpften Personen in Deutschlands an beliebigem Tag seit dem 27. Dezember 2020.
#'
#' @param Datum Ein Datum, default ist der aktuellste Tag aus der Impftabelle.
#' @return Ein Tibble mit Quote der Erstimpfung und der vollständigen Impfung.
#' @examples
#' vacc_quo()
#' vacc_quo(as.Date("2021-07-31"))
#' @export

vacc_quo <- function(Datum = tbl_vacc_raw$date[nrow(tbl_vacc_raw)]){
  stopifnot("Objekt der Klasse Datum eingeben!" = (class(Datum) == "Date"))

  tbl_vacc_raw %>%
    filter(date == Datum) %>%
    select(impf_quote_erst, impf_quote_voll) -> quote
    if (nrow(quote) == 0) stop("Keine Daten zu diesem Tag vorhanden")
    cat("Mind. einmal geimpft:", round(quote$impf_quote_erst*100, digits=1), "%\nVollständig geimpft:", round(quote$impf_quote_voll*100, digits=1), "%")
    invisible(quote)
}



################################################################################
#' Bearbeitung Impftabellen
#'
#' Kumulative Anzahl der Impfungen seit dem 27. Dezember 2020, sortiert nach Impfstoff und Impfstadium (erst, zweit, voll).
#' Hierbei ist zu beachten, dass Zweit- und vollständige Impfung nicht synonym zu verwenden sind,
#' da zum Beispiel bei Johnson&Johnson oder für Genesene eine einzige Impfung für vollständigen Impfschutz aussreicht.
#'
#' @param Impfstoff Ein String mit dem gewünschtem Impfstoff (mindestens die ersten vier Buchstaben),
#' \code{"alle"} für aufgeteilt in alle Impfstoffe, \code{"gesamt"} (default) für Gesamtanzahl, d.h. unabhängig von Impfstoff.
#' @param Impfung Ein String mit dem Impfstadium,
#' \code{"voll"} für Erst-& vollständige Impfung (nur für \code{Impfstoff="gesamt"} verfügbar),
#' \code{"erst"}, \code{"zweit"} oder \code{"erst|zweit"} für Erst- und/oder Zweitimpfung, \code{"gesamt"} ((default) für Gesamtanzahl, unabhängig von Impfstadium.
#'
#' @return Ein Tibble, strukturiert nach gewähltem Impfstoff und Impfstadium.
#'
#' @seealso \code{plot_vacc()}
#'
#' @examples
#' vacc_convert()
#' vacc_convert(Impfung="voll")
#' vacc_convert(Impfstoff="alle", Impfung="zweit")
#' vacc_convert(Impfstoff="Johnson&Johnson")
#' vacc_convert(Impfstoff="Astra", Impfung="erst|zweit")
#' @export

vacc_convert <- function(Impfstoff="gesamt", Impfung="gesamt"){
  tbl_vacc_raw %>%
    rename("Datum" = date) %>%
    rename("dosen_johnson_erst_kumulativ" = dosen_johnson_kumulativ) -> tbl_vacc

  #kein spezifischer Impfstoff
  if (Impfstoff == "gesamt") {
    tbl_vacc %>%
      select(Datum, dosen_kumulativ, dosen_erst_kumulativ, dosen_zweit_kumulativ) -> tbl_vacc

  #alle Impfstoffe
  } else if (Impfstoff == "alle") {
      tbl_vacc %>%
        select(Datum, str_subset(colnames(tbl_vacc), "biontech|moderna|astra|johnson")) -> tbl_vacc

  #spezifischer Impfstoff
  } else {
    tbl_vacc %>%
      select(Datum, str_subset(colnames(tbl_vacc), str_sub(str_to_lower(Impfstoff), end=4))) -> tbl_vacc

    stopifnot("Kein Impfstoff mit diesem Namen!" = (ncol(tbl_vacc) > 1))
  }

  #gesamte Impfungen
  if (Impfung == "gesamt") {

    if (any(str_detect(colnames(tbl_vacc), "johnson"))) {
      tbl_vacc %>%
        select(str_subset(colnames(tbl_vacc), "erst|zweit", negate=TRUE), dosen_johnson_erst_kumulativ) -> tbl_vacc
    } else {
      tbl_vacc %>%
        select(str_subset(colnames(tbl_vacc), "erst|zweit", negate=TRUE)) -> tbl_vacc
    }

  #Erst-&Vollimpfung
  } else if (Impfung == "voll") {
    stopifnot("Daten zu vollständigem Impfschutz nicht für einzelne Impfstoffe verfügbar!" = (Impfstoff == "gesamt"))
    tbl_vacc %>%
      summarize(Datum, mind_1_geimpft = round(EinwohnerDeutschlandGesamt*tbl_vacc_raw$impf_quote_erst), voll_geimpft = round(EinwohnerDeutschlandGesamt*tbl_vacc_raw$impf_quote_voll)) -> tbl_vacc

  #Erst- und/oder Zweitimpfung
  } else {
    tbl_vacc %>%
      select(Datum, str_subset(colnames(tbl_vacc), Impfung)) -> tbl_vacc

    stopifnot('Gültiges Impfstadium ("voll", "erst", "zweit" oder "erst|zweit") angeben!' = (ncol(tbl_vacc) > 1 ))
  }
  tbl_vacc
}



################################################################################
#' Impfplots
#'
#' Plots zum zeitlichen Verlauf des Impfgeschehens.
#' Entweder Gegenüberstellung von verschiedenen Impfstoffen oder Impfstadien.
#' Beides gleichzeitig ist nicht möglich.
#'
#' @param Tabelle Ein Tibble mit Impfdaten, erstellt mit \code{vacc_convert()}.
#' @param interaktiv Logischer Indikator, ob Plot mit ggplot oder interaktiv mit plotly (default) ausgegeben werden soll.
#' @param Titel Ein String als Titel für den Plot.
#' @param Untertitel Ein String als Untertitel für den Plot. Wird bei \code{interaktiv = TRUE} nicht angezeigt.
#'
#' @return Ein ggplot oder plotly.
#'
#' @seealso \code{vacc_convert()}
#'
#' @examples
#' plot_vacc()
#' plot_vacc(vacc_convert(Impfstoff="alle"))
#' plot_vacc(vacc_convert(Impfstoff="alle", Impfung="zweit"))
#' plot_vacc(vacc_convert(Impfstoff="Moderna", Impfung="erst|zweit"))
#' plot_vacc(vacc_convert(Impfstoff="Astra", Impfung="erst|zweit"))
#' @export

plot_vacc <- function(Tabelle=vacc_convert(Impfung="voll"), interaktiv=TRUE, Titel = NULL, Untertitel = NULL) {
  #Erst-&Vollimpfung
  if (any(str_detect(colnames(Tabelle), "geimpft"))) {
    ggplot(Tabelle, aes(x=Datum)) +
      geom_area(aes(y=mind_1_geimpft, fill = "mind. einmal geimpft"), alpha = 0.7) +
      geom_area(aes(y=voll_geimpft, fill = "vollständig geimpft"), alpha = 0.7) +
      geom_line(aes(y=EinwohnerDeutschlandGesamt), alpha = 0.4) +
      guides(fill=guide_legend(title=NULL)) +
      labs(y = "Anzahl",
           title = Titel,
           subtitle = Untertitel) +
      plot_design -> p
    if (interaktiv) {ggplotly(p)} else {p}

  #Erst-&Zweitimpfung
  } else if (any(str_detect(colnames(Tabelle), "erst")) & any(str_detect(colnames(Tabelle), "zweit"))) {
    #Verschiedene Impfstoffe
    if (Tabelle %>%
        select(-Datum) %>%
        colnames() %>%
        str_extract_all("biontech|moderna|astra|johnson") %>%
        unique() %>%
        length() != 1 ) {
      stop("Kein Plot für verschiedene Impfstoffe und Impfstadien gleichzeitig!")

    #kein oder ein spezifischer Impfstoff
    } else {
    Tabelle %>%
      pivot_longer(2:ncol(Tabelle), names_to = "Impfstoff", values_to = "Anzahl") -> Tabelle

    ggplot(Tabelle, aes(x=Datum)) +
      geom_area(aes(y=Anzahl, fill=Impfstoff), alpha=0.7, position="identity") +
      guides(fill=guide_legend(title=NULL)) +
      plot_design -> p
    if (interaktiv) {ggplotly(p)} else {p}
    }

  #gesamte Impfungen
  } else {
    Tabelle %>%
      pivot_longer(2:ncol(Tabelle), names_to = "Impfstoff", values_to = "Anzahl") -> Tabelle

    ggplot(Tabelle, aes(x=Datum)) +
      geom_area(aes(y=Anzahl, fill=Impfstoff), alpha=0.7) +
      guides(fill=guide_legend(title=NULL)) +
      labs(title = Titel,
           subtitle = Untertitel) +
      plot_design -> p
    if (interaktiv) {ggplotly(p)} else {p}
  }
}
